<!ENTITY torsettings.dialog.title "Nettverksinnstillinger for Tor">
<!ENTITY torsettings.wizard.title.default "Koble til Tor">
<!ENTITY torsettings.wizard.title.configure "Nettverksinnstillinger for Tor">
<!ENTITY torsettings.wizard.title.connecting "Etablering av tilkobling">

<!-- For locale picker: -->
<!ENTITY torlauncher.localePicker.title "Språk i Tor-nettleseren">
<!ENTITY torlauncher.localePicker.prompt "Vennligst velg et språk">

<!-- For "first run" wizard: -->

<!ENTITY torSettings.connectPrompt "Klikk &quot;koble til&quot; for å koble til Tor.">
<!ENTITY torSettings.configurePrompt "Klikk &quot;Sett opp&quot; for å justere nettverksinnstillinger hvis du er i et land som sensurerer Tor (som Egypt, Kina, Tyrkia,) eller hvis du kobler til fra et privat nettverk som krever en mellomtjener.">
<!ENTITY torSettings.configure "Sett opp">
<!ENTITY torSettings.connect "Koble til">

<!-- Other: -->

<!ENTITY torsettings.startingTor "Venter på at Tor skal starte…">
<!ENTITY torsettings.restartTor "Omstart av Tor">
<!ENTITY torsettings.reconfigTor "Sett opp på ny">

<!ENTITY torsettings.discardSettings.prompt "Du har satt opp Tor-broer eller du har angitt lokale mellomtjeningsinnstillinger. &#160; For å gjøre en direkte forbindelse til Tor-nettverket, må disse innstillingene fjernes.">
<!ENTITY torsettings.discardSettings.proceed "Fjern innstillinger og koble til">

<!ENTITY torsettings.optional "Valgfritt">

<!ENTITY torsettings.useProxy.checkbox "Jeg bruker en mellomtjener, for å koble til Internett">
<!ENTITY torsettings.useProxy.type "Mellomtjener-type">
<!ENTITY torsettings.useProxy.type.placeholder "velg en mellomtjener-type">
<!ENTITY torsettings.useProxy.address " Adresse">
<!ENTITY torsettings.useProxy.address.placeholder "IP-adresse, eller vertsnavn">
<!ENTITY torsettings.useProxy.port "Port">
<!ENTITY torsettings.useProxy.username "Brukernavn">
<!ENTITY torsettings.useProxy.password "Passord">
<!ENTITY torsettings.useProxy.type.socks4 "SOCKS 4">
<!ENTITY torsettings.useProxy.type.socks5 "SOCKS 5">
<!ENTITY torsettings.useProxy.type.http "HTTP / HTTPS">
<!ENTITY torsettings.firewall.checkbox "Denne datamaskinen går gjennom en brannmur som kun tillater tilkoblinger til visse porter">
<!ENTITY torsettings.firewall.allowedPorts "Tillatte porter">
<!ENTITY torsettings.useBridges.checkbox "Tor er sensurert i mitt land">
<!ENTITY torsettings.useBridges.default "Velg en bro med flere innfartsårer">
<!ENTITY torsettings.useBridges.default.placeholder "velg en bro">
<!ENTITY torsettings.useBridges.bridgeDB "Forespør en bro fra torproject.org">
<!ENTITY torsettings.useBridges.captchaSolution.placeholder "Skriv inn tegnene fra bildet">
<!ENTITY torsettings.useBridges.reloadCaptcha.tooltip "Få en ny utfordring">
<!ENTITY torsettings.useBridges.captchaSubmit "Send inn">
<!ENTITY torsettings.useBridges.custom "Tilby en bro jeg kjenner">
<!ENTITY torsettings.useBridges.label "Skriv inn broinformasjon fra en betrodd kilde.">
<!ENTITY torsettings.useBridges.placeholder "type addresse:port (én per linje)">

<!ENTITY torsettings.copyLog "Kopier Tor-loggen til utklippstavlen">

<!ENTITY torsettings.proxyHelpTitle "Mellomtjenerhjelp">
<!ENTITY torsettings.proxyHelp1 "En lokal mellomtjener kan være nødvendig når du kobler deg gjennom et firma, skole eller universitet nettverk.&#160; Hvis du ikke er sikker på om en mellomtjener er nødvendig, kan du se på Internett-innstillingene i en annen nettleser eller kontrollere systemets nettverksinnstillinger.">

<!ENTITY torsettings.bridgeHelpTitle "Hjelp med Bro-reléoppsett">
<!ENTITY torsettings.bridgeHelp1 "Broer er ulistede rutingstafettoppsett som gjør det vanskeligere å blokkere tilkoblinger til Tor-nettverket.&#160; Hver type bro bruker en egen måte å unngå sensur.&#160; Obfs-broene får trafikken din til å se ut som tilfeldig støy, og de ydmyke broene får broene til å se ut som de kobler til en annen tjeneste enn Tor.">
<!ENTITY torsettings.bridgeHelp2 "På grunn av hvordan noen land prøver å blokkere Tor, fungerer noen broer i noen land, men ikke i andre..&#160;Hvis du er usikker på hvilke broer som fungerer i ditt land, besøk torproject.org/about/contact.html#support">

<!-- Progress -->
<!ENTITY torprogress.pleaseWait "Vent mens tilkobling til Tor-nettverket opprettes.&#160; Dette kan ta flere minutter.">

<!-- #31286 about:preferences strings -->
<!ENTITY torPreferences.categoryTitle "Tilkobling">
<!ENTITY torPreferences.torSettings "Tor-innstillinger">
<!ENTITY torPreferences.torSettingsDescription "Tor Browser dirigerer trafikken din over Tor-nettverket, drevet av tusenvis av frivillige rundt om i verden." >
<!ENTITY torPreferences.learnMore "Lær mer">
<!-- Status -->
<!ENTITY torPreferences.statusInternetLabel "Internet:">
<!ENTITY torPreferences.statusInternetTest "Test">
<!ENTITY torPreferences.statusInternetOnline "Tilkoblet">
<!ENTITY torPreferences.statusInternetOffline "Frakoblet">
<!ENTITY torPreferences.statusTorLabel "Tor Network:">
<!ENTITY torPreferences.statusTorConnected "Tilkoblet">
<!ENTITY torPreferences.statusTorNotConnected "Not Connected">
<!ENTITY torPreferences.statusTorBlocked "Potentially Blocked">
<!ENTITY torPreferences.learnMore "Lær mer">
<!-- Quickstart -->
<!ENTITY torPreferences.quickstart "Quickstart">
<!ENTITY torPreferences.quickstartDescriptionLong "Quickstart connects Tor Browser to the Tor Network automatically when launched, based on your last used connection settings.">
<!ENTITY torPreferences.quickstartCheckbox "Always connect automatically">
<!-- Bridge settings -->
<!ENTITY torPreferences.bridges "Broer">
<!ENTITY torPreferences.bridgesDescription "Broer hjelper deg med å få tilgang til Tor-nettverket på steder der Tor er blokkert. Avhengig av hvor du er, kan det hende at en bro fungerer bedre enn en annen.">
<!ENTITY torPreferences.bridgeLocation "Your location">
<!ENTITY torPreferences.bridgeLocationAutomatic "Automatisk">
<!ENTITY torPreferences.bridgeLocationFrequent "Frequently selected locations">
<!ENTITY torPreferences.bridgeLocationOther "Other locations">
<!ENTITY torPreferences.bridgeChooseForMe "Choose a Bridge For Me…">
<!ENTITY torPreferences.bridgeBadgeCurrent "Your Current Bridges">
<!ENTITY torPreferences.bridgeBadgeCurrentDescription "You can save one or more bridges, and Tor will choose which one to use when you connect. Tor will automatically switch to use another bridge when needed.">
<!ENTITY torPreferences.bridgeId "#1 bridge: #2"> <!-- #1 = bridge type; #2 = bridge emoji id -->
<!ENTITY torPreferences.remove "Fjern">
<!ENTITY torPreferences.bridgeDisableBuiltIn "Disable built-in bridges">
<!ENTITY torPreferences.bridgeShare "Share this bridge using the QR code or by copying its address:">
<!ENTITY torPreferences.bridgeCopy "Copy Bridge Address">
<!ENTITY torPreferences.copied "Kopiert!">
<!ENTITY torPreferences.bridgeShowAll "Show All Bridges">
<!ENTITY torPreferences.bridgeRemoveAll "Remove All Bridges">
<!ENTITY torPreferences.bridgeAdd "Add a New Bridge">
<!ENTITY torPreferences.bridgeSelectBrowserBuiltin "Choose from one of Tor Browser’s built-in bridges">
<!ENTITY torPreferences.bridgeSelectBuiltin "Select a Built-In Bridge…">
<!ENTITY torPreferences.bridgeRequest "Forespør en bro…">
<!ENTITY torPreferences.bridgeEnterKnown "Enter a bridge address you already know">
<!ENTITY torPreferences.bridgeAddManually "Add a Bridge Manually…">
<!-- Advanced settings -->
<!ENTITY torPreferences.advanced "Avansert">
<!ENTITY torPreferences.advancedDescription "Configure how Tor Browser connects to the internet">
<!ENTITY torPreferences.advancedButton "Settings…">
<!ENTITY torPreferences.viewTorLogs "View the Tor logs">
<!ENTITY torPreferences.viewLogs "Vis logger...">
<!-- Remove all bridges dialog -->
<!ENTITY torPreferences.removeBridgesQuestion "Remove all the bridges?">
<!ENTITY torPreferences.removeBridgesWarning "This action cannot be undone.">
<!ENTITY torPreferences.cancel "Avbryt">
<!-- Scan bridge QR dialog -->
<!ENTITY torPreferences.scanQrTitle "Scan the QR code">
<!-- Builtin bridges dialog -->
<!ENTITY torPreferences.builtinBridgeTitle "Built-In Bridges">
<!ENTITY torPreferences.builtinBridgeDescription "Tor Browser includes some specific types of bridges known as “pluggable transports”.">
<!ENTITY torPreferences.builtinBridgeObfs4 "obfs4">
<!ENTITY torPreferences.builtinBridgeObfs4Description "obfs4 is a type of built-in bridge that makes your Tor traffic look random. They are also less likely to be blocked than their predecessors, obfs3 bridges.">
<!ENTITY torPreferences.builtinBridgeSnowflake "Snowflake">
<!ENTITY torPreferences.builtinBridgeSnowflakeDescription "Snowflake is a built-in bridge that defeats censorship by routing your connection through Snowflake proxies, ran by volunteers.">
<!ENTITY torPreferences.builtinBridgeMeekAzure "meek-azure">
<!ENTITY torPreferences.builtinBridgeMeekAzureDescription "meek-azure is a built-in bridge that makes it look like you are using a Microsoft web site instead of using Tor.">
<!-- Request bridges dialog -->
<!ENTITY torPreferences.requestBridgeDialogTitle "Forespør en bro">
<!ENTITY torPreferences.requestBridgeDialogWaitPrompt "Kontakter BridgeDB. Vennligst vent.">
<!ENTITY torPreferences.requestBridgeDialogSolvePrompt "Løs CAPTCHA-en for å forespørre en bro.">
<!ENTITY torPreferences.requestBridgeErrorBadSolution "Løsningen er ikke riktig. Vær så snill, prøv på nytt.">
<!-- Provide bridge dialog -->
<!ENTITY torPreferences.provideBridgeTitle "Provide Bridge">
<!ENTITY torPreferences.provideBridgeHeader "Enter bridge information from a trusted source">
<!-- Connection settings dialog -->
<!ENTITY torPreferences.connectionSettingsDialogTitle "Connection Settings">
<!ENTITY torPreferences.connectionSettingsDialogHeader "Configure how Tor Browser connects to the Internet">
<!ENTITY torPreferences.firewallPortsPlaceholder "Comma-seperated values">
<!-- Log dialog -->
<!ENTITY torPreferences.torLogsDialogTitle "Tor-logger">

<!-- #24746 about:torconnect strings -->
<!ENTITY torConnect.notConnectedConcise "Not Connected">
<!ENTITY torConnect.connectingConcise "Kobler til...">
<!ENTITY torConnect.tryingAgain "Trying again…">
<!ENTITY torConnect.noInternet "Tor Browser couldn’t reach the Internet">
<!ENTITY torConnect.noInternetDescription "This could be due to a connection issue rather than Tor being blocked. Check your Internet connection, proxy and firewall settings before trying again.">
<!ENTITY torConnect.couldNotConnect "Tor Browser could not connect to Tor">
<!ENTITY torConnect.assistDescriptionConfigure "configure your connection"> <!-- used as a text to insert as a link on several strings (#1) -->
<!ENTITY torConnect.assistDescription "If Tor is blocked in your location, trying a bridge may help. Connection assist can choose one for you using your location, or you can #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.tryingBridge "Trying a bridge…">
<!ENTITY torConnect.tryingBridgeAgain "Trying one more time…">
<!ENTITY torConnect.errorLocation "Tor Browser couldn’t locate you">
<!ENTITY torConnect.errorLocationDescription "Tor Browser needs to know your location in order to choose the right bridge for you. If you’d rather not share your location, #1 manually instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.isLocationCorrect "Are these location settings correct?">
<!ENTITY torConnect.isLocationCorrectDescription "Tor Browser still couldn’t connect to Tor. Please check your location settings are correct and try again, or #1 instead."> <!-- #1 = "configure your connection" link -->
<!ENTITY torConnect.finalError "Tor Browser still cannot connect">
<!ENTITY torConnect.finalErrorDescription "Despite its best efforts, connection assist was not able to connect to Tor. Try troubleshooting your connection and adding a bridge manually instead.">
<!ENTITY torConnect.breadcrumbAssist "Connection assist">
<!ENTITY torConnect.breadcrumbLocation "Location settings">
<!ENTITY torConnect.breadcrumbTryBridge "Try a bridge">
<!ENTITY torConnect.automatic "Automatisk">
<!ENTITY torConnect.selectCountryRegion "Select Country or Region">
<!ENTITY torConnect.frequentLocations "Frequently selected locations">
<!ENTITY torConnect.otherLocations "Other locations">
<!ENTITY torConnect.restartTorBrowser "Restart Tor Browser">
<!ENTITY torConnect.configureConnection "Configure Connection…">
<!ENTITY torConnect.viewLog "View logs…">
<!ENTITY torConnect.tryAgain "Prøv igjen">
<!ENTITY torConnect.offline "Internet not reachable">
<!ENTITY torConnect.connectMessage "Changes to Tor Settings will not take effect until you connect">
<!ENTITY torConnect.tryAgainMessage "Tor Browser has failed to establish a connection to the Tor Network">
<!ENTITY torConnect.yourLocation "Your Location">
<!ENTITY torConnect.tryBridge "Try a Bridge">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.autoBootstrappingFailed "Automatic configuration failed">
<!ENTITY torConnect.cannotDetermineCountry "Unable to determine user country">
<!ENTITY torConnect.noSettingsForCountry "No settings available for your location">
