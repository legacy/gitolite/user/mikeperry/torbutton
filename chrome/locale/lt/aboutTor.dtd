<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Apie Tor">

<!ENTITY aboutTor.viewChangelog.label "Rodyti keitinių žurnalą">

<!ENTITY aboutTor.ready.label "Naršykite. Privačiai.">
<!ENTITY aboutTor.ready2.label "Jūs esate pasiruošę privačiausiam pasaulyje naršymo potyriui.">
<!ENTITY aboutTor.failure.label "Kažkas nutiko!">
<!ENTITY aboutTor.failure2.label "Šioje naršyklėje Tor neveikia.">

<!ENTITY aboutTor.search.label "Ieškoti naudojant DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Turite klausimų?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Peržiūrėkite mūsų Tor Naršyklės naudotojo vadovą »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "V">
<!ENTITY aboutTor.torbrowser_user_manual.label "Tor Naršyklės naudotojo vadovas">

<!ENTITY aboutTor.tor_mission.label "Tor Project yra JAV 501(c)(3) ne pelno organizacija remianti žmogaus teises ir laisves, kurianti ir diegianti laisvas ir atvirojo kodo anonimiškumo bei privatumo technologijas, palaikanti jų neribotą prieinamumą ir naudojimą bei mokslinį ir visuotinį supratimą.">
<!ENTITY aboutTor.getInvolved.label "Įsitraukite »">

<!ENTITY aboutTor.newsletter.tagline "Gaukite naujienas iš Tor tiesiai į savo pašto dėžutę.">
<!ENTITY aboutTor.newsletter.link_text "Užsisakykite Tor naujienas.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor yra nemokamas naudoti dėl paaukojimų iš tokių žmonių kaip jūs.">
<!ENTITY aboutTor.donationBanner.buttonA "Paaukokite dabar">

<!ENTITY aboutTor.alpha.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "PAAUKOKITE DABAR">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "„Friends of Tor“ paaukos tiek pat, kiek ir jūs, iki $100000.">
