<!--
   - Copyright (c) 2019, The Tor Project, Inc.
   - See LICENSE for licensing information.
   - vim: set sw=2 sts=2 ts=8 et syntax=xml:
  -->

<!ENTITY aboutTor.title "Over Tor">

<!ENTITY aboutTor.viewChangelog.label "Wijzigingslogboek bekijken">

<!ENTITY aboutTor.ready.label "Verken. Privé.">
<!ENTITY aboutTor.ready2.label "U bent klaar voor de meest private surfervaring ter wereld.">
<!ENTITY aboutTor.failure.label "Er ging iets mis!">
<!ENTITY aboutTor.failure2.label "Tor werkt niet in deze browser.">

<!ENTITY aboutTor.search.label "Zoeken met DuckDuckGo">
<!ENTITY aboutTor.searchDDGPost.link "https://duckduckgo.com/">

<!ENTITY aboutTor.torbrowser_user_manual_questions.label "Vragen?">
<!ENTITY aboutTor.torbrowser_user_manual_link.label "Bekijk onze Tor-browserhandleiding »">
<!-- The next two entities are used within the browser's Help menu. -->
<!ENTITY aboutTor.torbrowser_user_manual.accesskey "H">
<!ENTITY aboutTor.torbrowser_user_manual.label "Tor-browserhandleiding">

<!ENTITY aboutTor.tor_mission.label "Het Tor Project is een 501(c)(3)-non-profitorganisatie in de VS die rechten en vrijheden van de mens bevordert door vrije en open source anonimiteits- en privacytechnologieën te ontwikkelen en te implementeren, de onbeperkte beschikbaarheid en het gebruik ervan te steunen, en het begrip ervan in de wetenschap en bij het algemeen publiek te bevorderen.">
<!ENTITY aboutTor.getInvolved.label "Doe mee »">

<!ENTITY aboutTor.newsletter.tagline "Ontvang het laatste nieuws van Tor direct in uw postvak.">
<!ENTITY aboutTor.newsletter.link_text "Meld u aan voor de Tor-nieuwsbrief.">
<!ENTITY aboutTor.donationBanner.freeToUse "Tor is gratis te gebruiken dankzij donaties van mensen zoals u.">
<!ENTITY aboutTor.donationBanner.buttonA "Doneer nu">

<!ENTITY aboutTor.alpha.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.alpha.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.alpha.bannerDescription "Tor Browser Alpha is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.alpha.bannerLink "Report a bug on the Tor Forum">

<!ENTITY aboutTor.nightly.ready.label "Test. Thoroughly.">
<!ENTITY aboutTor.nightly.ready2.label "You’re ready to test the world’s most private browsing experience.">
<!ENTITY aboutTor.nightly.bannerDescription "Tor Browser Nightly is an unstable version of Tor Browser you can use to preview new features, test their performance and provide feedback before release.">
<!ENTITY aboutTor.nightly.bannerLink "Report a bug on the Tor Forum">

<!-- YEC 2022 campaign https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303 -->
<!-- LOCALIZATION NOTE (aboutTor.yec2022.powered_by_privacy): a header for a list of things which are powered by/enabled by/possible due to privacy (each item should have positive connotations/associations in the translated languages) -->
<!ENTITY aboutTor.yec2022.powered_by_privacy "POWERED BY PRIVACY:">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.resistance): resistance as in social/political resistance to opression, injustice, etc -->
<!ENTITY aboutTor.yec2022.resistance "RESISTANCE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.change): change as in the process of social/political progress toward a better/more equitable society -->
<!ENTITY aboutTor.yec2022.change "CHANGE">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.freedom): freedom as in liberty, protection against exploition, imprisonment, etc -->
<!ENTITY aboutTor.yec2022.freedom "FREEDOM">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donate_now): Label for a button directing user to donation page-->
<!ENTITY aboutTor.yec2022.donate_now "NU DONEREN">
<!-- LOCALIZATION NOTE (aboutTor.yec2022.donation_matching): Please translate the 'Friends of Tor' phrase, but
also format it like the name of an organization in whichever way that is appropriate for your locale.

Please keep the currency in USD.
Thank you!
-->
<!ENTITY aboutTor.yec2022.donation_matching "Uw donatie wordt verdubbeld door Friends of Tor, tot $100.000.">
